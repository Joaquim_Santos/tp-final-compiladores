%{
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

int yylex();
extern int quant_linhas;
int yyerror(char *s);
extern FILE * yyin;

%}

/*Palavras reservadas da linguagem*/
%token LBEGIN 
%token BOOLEAN		
%token CHAR		
%token DO		
%token ELSE		
%token END		
%token ENDIF
%token ENDWHILE
%token EXIT
%token FALSE
%token IF
%token INTEGER
%token PROCEDURE
%token PROGRAM
%token READ			
%token REFERENCE	
%token REPEAT	
%token RETURN		
%token THEN		
%token TRUE
%token TYPE		
%token UNTIL		
%token VALUE				
%token WHILE
%token WRITE
/*Operadores aritméticos da linguagem*/
%token SUM 
%token SUB 
%token MUL 
%token DIV 
%token POW 
%token UNARY	
/*Operadores relacionais da linguagem*/ 
%token LT
%token GT
%token LE
%token GE
%token EQ
%token NE 
/*Operadores booleanos da linguagem*/ 
%token OR 
%token AND
%token NOT
/*Identificadores e número inteiros e decimais*/
%token ID 
%token NUM
/*Caracteres específicos da linguagem, destinados a outras funcionalidades*/
%token OPENP  
%token CLOSEP
%token ENDLINE
%token ASSIGN 
%token DEFTYPE 
%token SEPAR   
%token LEXERROR
/*Precedências e associatividades dos operadores da Órion*/
%left SUM  
%left SUB  
%left MUL  
%left DIV  
%right POW 
%left UNARY 
%left LT   
%left GT
%left LE   
%left GE   
%left EQ     
%left NE
%left OR  
%left AND  
%left NOT 

%%

program                 :  PROGRAM vazio0 declaracoes vazio1 bloco {printf("\n Sintzxe do programa está totalmente correta!!! \n");};
bloco                   :  LBEGIN lista_comandos vazio0 END;
vazio                   :  ;
vazio0                  :  vazio;
vazio1                  :  vazio;
declaracoes             :  declaracoes vazio0 declaracao ENDLINE | vazio;
declaracao              :  declaracao_variavel | definicao_tipo | declaracao_procedimento;
tipo                    :  INTEGER |  BOOLEAN |  CHAR | tipo_composto; 
tipo_composto           :  identificador;
identificador           :  ID;
definicao_tipo          :  TYPE identificador vazio0 EQ vazio1 definicao_composta;
definicao_composta      :  OPENP delimitadores CLOSEP tipo;
delimitadores           :  NUM DEFTYPE NUM | delimitadores  SEPAR NUM DEFTYPE NUM;
declaracao_variavel     :  tipo DEFTYPE lista_identificadores;
lista_identificadores   :  identificador | lista_identificadores SEPAR identificador;
declaracao_procedimento :  cabecalho_procedimento corpo_procedimento;
cabecalho_procedimento  :  tipo_retorno PROCEDURE vazio0 identificador conjunto_parametros;
tipo_retorno            :  INTEGER | BOOLEAN | CHAR | vazio;
conjunto_parametros     :  OPENP lista_parametros CLOSEP | vazio;
lista_parametros        :  parametro | lista_parametros SEPAR parametro;  
parametro               :  modo_passagem tipo DEFTYPE identificador;
modo_passagem           :  VALUE | REFERENCE;
corpo_procedimento      :  DEFTYPE declaracoes vazio0 bloco vazio1 | vazio;
lista_comandos          :  comando | lista_comandos ENDLINE vazio0 comando;
comando                 :  comando_atribuicao | comando_exit | comando_if | comando_read | comando_repeat | comando_return | comando_while | comando_write | chamada_procedimento | identificador DEFTYPE comando | vazio;
comando_atribuicao      :  variavel ASSIGN expressao;
variavel                :  identificador | indexacao;
indexacao               :  indice CLOSEP;
indice                  :  identificador OPENP expressao | indice SEPAR expressao;
expressao               :  expressao OR vazio expressao | expressao AND vazio expressao | NOT expressao | expressao LT expressao | expressao GT expressao | expressao EQ expressao | expressao LE expressao | expressao GE expressao | expressao NE expressao | expressao SUM expressao | expressao SUB expressao | expressao MUL expressao | expressao DIV expressao | expressao POW expressao | SUB expressao %prec UNARY | variavel | constante | OPENP expressao CLOSEP;
constante               :  TRUE | FALSE | NUM | CHAR;          
chamada_procedimento    :  identificador | indexacao;
comando_exit            :  EXIT identificador;
comando_if              :  IF expressao THEN vazio lista_comandos ENDIF | IF expressao THEN vazio lista_comandos vazio0 ELSE vazio1 lista_comandos ENDIF;
comando_read            :  READ variavel;
comando_repeat          :  REPEAT vazio0 lista_comandos UNTIL vazio1 expressao;
comando_return          :  RETURN expressao;
comando_while           :  WHILE vazio0 expressao DO vazio1 lista_comandos ENDWHILE;
comando_write           :  WRITE expressao;

%%

int yyerror(char *s){
  fprintf(stderr, "%s\n", s);
  printf("\n Erro detectado durante Analise Sintatica  na linha: %d\n", quant_linhas);
  return 0;
}


extern int yylex();
extern int yyparse();

int main(void){  
        const char *nome_arquivo;
        
        printf("\n =========================== Analisador Sintatico e Lexico para a Linguagem Orion =========================== \n\n");
        printf("Informe o nome do arquivo de entrada para submeter a analise: ");
        scanf ("%s",nome_arquivo); 
        FILE *arquivo = fopen(nome_arquivo, "r");
	if (!arquivo) {
		printf("Erro na Leitura do Arquivo! Nao Foi Possivel Abrir!\n\n");
		return -1;
	}
	// Definindo para o lex ler a entrada do arquivo
	yyin = arquivo;
	
	// Analisando o arquivo de entrada até o final
	do {
		yyparse();
	} while (!feof(yyin));

	return 0;
}
