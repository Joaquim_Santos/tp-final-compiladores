%{ 
   /* variável para arazenar o número de linhas analisadas */
   int quant_linhas;
   #include<stdio.h>
   #include <stdlib.h>
   #include <string.h>
%} 

%option noyywrap 

/* Definições Regulares */ 

espacamento 			[ \t]
espacamentos            {espacamento}+
digito			        [0-9]
letra                   [A-Za-z]
operador_arit           (\+|\-|\*|\/|\*\*)
operador_rel            (>|<|>=|<=|=|not=)
operador_bool           [\&\|not]
inteiro_positivo 	    {digito}+
inteiro_negativo 	    \-{digito}+
numero_decimal 		    {digito}+[.]{digito}+
palavra_chave           (begin|boolean|char|do|else|end|false|endif|endwhile|exit|if|integer|procedure|program|reference|repeat|read|return|then|true|type|until|value|write|while)
identificador 			{letra}({letra}|{digito})*
operador_unario 		{identificador}\-

%% 

{espacamentos}          {/* Nada a se fazer, apenas ignora */}
\n                      {quant_linhas++;}
"/*"[^\n]*"*/"		     /* comentarios sao ignorados */
{operador_arit}         {printf("Linha [%i] com Operador Aritmetico: %s\n",quant_linhas, yytext);}    
{operador_rel}          {printf("Linha [%i] com Operador Relacional: %s\n",quant_linhas, yytext);}
{operador_bool}         {printf("Linha [%i] com Operador Booleano: %s\n",quant_linhas, yytext);}
{operador_unario}	    {printf("Linha [%i] com Operador unario: %s\n",quant_linhas, yytext);} 
{inteiro_positivo}      {printf("Linha [%i] com Numero Inteiro Positivo: %s\n",quant_linhas, yytext);} 
{inteiro_negativo}      {printf("Linha [%i] com Numero Inteiro Negativo: %s\n",quant_linhas, yytext);}
{numero_decimal}        {printf("Linha [%i] com Numero Decimal: %s\n",quant_linhas, yytext);}
{palavra_chave}         {printf("Linha [%i] com Palavra-Chave: %s\n",quant_linhas, yytext);}
{identificador}         {printf("Linha [%i] com Identificador: %s\n",quant_linhas, yytext);}
"("			            {printf("Linha [%i] com Abertura de Parenteses: %s\n",quant_linhas, yytext);}
")"			            {printf("Linha [%i] com Fechamento de Parenteses: %s\n",quant_linhas, yytext);}
";"			            {printf("Linha [%i] com Operador de Final de linha: %s\n",quant_linhas, yytext);}
":=" 			        {printf("Linha [%i] com Operacao de Atribuicao de variavel: %s\n",quant_linhas, yytext);}
":"			            {printf("Linha [%i] com Operacao de Atribuicao de tipo: %s\n",quant_linhas, yytext);}
","			            {printf("Linha [%i] com Operador de separacao: %s\n",quant_linhas, yytext);}
. 			            {printf("Linha [%i] com algum Caractere Invalido para a linguagem! [%s]\n",quant_linhas,yytext);}

%% 

int main(void){
   
   int tipo_entrada;
   FILE *arquivo_entrada = NULL;
   char nome_arquivo[50];

   printf("Informe o tipo de entrada a ser realizada:\n1 - Ler do terminal\n2 - Ler do arquivo\nQualquer outro numero - Sair\n");
   scanf("%d", &tipo_entrada);
   
   if(tipo_entrada == 1){
      printf("Informe o texto de entrada:\n");
      yylex();	

   }
   else if (tipo_entrada == 2){
      printf("Informe o nome do arquivo de entrada: ");
      scanf("%s", nome_arquivo);
      arquivo_entrada = fopen(nome_arquivo,"rt");
      if(arquivo_entrada  == NULL){
        printf("Erro na leitura do arquivo!!!\n");
        exit(1);
      }
      yyset_in(arquivo_entrada);
      yylex();
      fclose(arquivo_entrada);	
   }   

   return 0;
}
