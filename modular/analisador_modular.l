%{ 
    #include "deftab.h"  /*todos os defines dos tokens utilizados (nomes e atributos)*/
    /* variável para arazenar o número de linhas analisadas */
    int quant_linhas;
    #include<stdio.h>
    #include <stdlib.h>
    #include <string.h>
    int ATRIBUTO;
%} 

%option noyywrap

/* Definições e expressões Regulares */ 

espacamento 			[ \t]
espacamentos            {espacamento}+
digito			        [0-9]
letra                   [A-Za-z]
numero                  {digito}+(\.{digito}+)?(E[\+\-]?{digito}+)?
identificador 			{letra}({letra}|{digito})*
operador_unario 		{identificador}\-

%% 

{espacamentos}          {/* Nada a se fazer, apenas ignora */}
\n                      {quant_linhas++;}
"/*"[^\n]*"*/"		     /* comentarios sao ignorados */
begin                   {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, LBEGIN, "");}
boolean                 {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, BOOLEAN, "");}
char                    {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, CHAR, "");}
do                      {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, DO, "");}
else                    {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, ELSE, "");}
end                     {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, END, "");}
false                   {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, FALSE, "");}
endif                   {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, ENDIF, "");}
endwhile                {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, ENDWHILE, "");}
exit                    {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, EXIT, "");}
if                      {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, IF, "");}
integer                 {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, INTEGER, "");}
procedure               {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, PROCEDURE, "");}
program                 {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, PROGRAM, "");}
reference               {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, REFERENCE, "");}
repeat                  {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, REPEAT, "");}
read                    {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, READ, "");}
return                  {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, RETURN, "");}
then                    {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, THEN, "");}
true                    {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, TRUE, "");}
type                    {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, TYPE, "");}
until                   {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, UNTIL, "");}
value                   {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, VALUE, "");}
write                   {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, WRITE, "");}
while                   {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, WHILE, "");}
"+"                     {ATRIBUTO = SUM; printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, AROP, ATRIBUTO);}
"-"                     {ATRIBUTO = SUB; printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, AROP, ATRIBUTO);}
"*"                     {ATRIBUTO = MUL; printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, AROP, ATRIBUTO);}
"/"                     {ATRIBUTO = DIV; printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, AROP, ATRIBUTO);}
"**"                    {ATRIBUTO = POW; printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, AROP, ATRIBUTO);}
"<"                     {ATRIBUTO = LT; printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, RELOP, ATRIBUTO);}
">"                     {ATRIBUTO = GT; printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, RELOP, ATRIBUTO);}
"<="                    {ATRIBUTO = LE; printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, RELOP, ATRIBUTO);}
">="                    {ATRIBUTO = GE; printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, RELOP, ATRIBUTO);}
"="                     {ATRIBUTO = EQ; printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, RELOP, ATRIBUTO);}
"not="                  {ATRIBUTO = NE; printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, RELOP, ATRIBUTO);}
"&"                     {ATRIBUTO = AND; printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, BOOLOP, ATRIBUTO);}
"|"                     {ATRIBUTO = OR;  printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, BOOLOP, ATRIBUTO);}
"not"                   {ATRIBUTO = NOT; printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, BOOLOP, ATRIBUTO);}
{operador_unario}		{printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, UNARY, "");}
{identificador} 	    {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, ID, yytext);}
{numero}	 	        {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, NUM, yytext);}
"("			            {ATRIBUTO = OPENP;   printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, OPENP, ATRIBUTO);}  
")"		                {ATRIBUTO = CLOSEP;  printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, CLOSEP, ATRIBUTO);} 	
";"		                {ATRIBUTO = ENDLINE; printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, ENDLINE, ATRIBUTO);}
":=" 			        {ATRIBUTO = ASSIGN;  printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, ASSIGN, ATRIBUTO);}  
":"			            {ATRIBUTO = DEFTYPE; printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, DEFTYPE, ATRIBUTO);}
","			            {ATRIBUTO = SEPAR;   printf ("Linha [%i] com token [nome,atributo] - [%i,%i]\n", quant_linhas, SEPAR, ATRIBUTO);}     	
. 			            {printf("Linha [%i] com algum Caractere Invalido para a linguagem! [%s]\n",quant_linhas,yytext);}

%% 

int main(void){
   
   int tipo_entrada;
   FILE *arquivo_entrada = NULL;
   char nome_arquivo[50];

   printf("Informe o tipo de entrada a ser realizada:\n1 - Ler do terminal\n2 - Ler do arquivo\nQualquer outro numero - Sair\n");
   scanf("%d", &tipo_entrada);
   
   if(tipo_entrada == 1){
      printf("Informe o texto de entrada:\n");
      yylex();	
   }
   else if (tipo_entrada == 2){
      printf("Informe o nome do arquivo de entrada: ");
      scanf("%s", nome_arquivo);
      arquivo_entrada = fopen(nome_arquivo,"rt");
      if(arquivo_entrada  == NULL){
        printf("Erro na leitura do arquivo!!!\n");
        exit(1);
      }
      yyset_in(arquivo_entrada);
      yylex();
      fclose(arquivo_entrada);	
   }   

   return 0;
}
